package auth

type PasswordHasher interface {
	Name() string
	Salt() string
	Encode(password, salt string) string
	Verify(password, encoded string) bool
	SafeSummery(encoded string) string
	MustUpdate(encoded string) bool
}
