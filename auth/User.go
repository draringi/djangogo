package auth

import (
	"time"
)

type User struct {
	Username, FirstName, LastName, Email, password string
	IsStaff, IsActive, IsSuperuser                 bool
	LastLogin, DateJoined                          time.Time
}
