package auth

import (
	"code.google.com/p/go.crypto/pbkdf2"
	"crypto/sha256"
	"encoding/base64"
	"fmt"
	"strconv"
)

type PBKDF2PasswordHasher struct {
	iterations int
}

func (self *PBKDF2PasswordHasher) Name() string {
	return "pbkdf2_sha256"
}

func (self *PBKDF2PasswordHasher) Encode(password, salt, iterations string) string {
	if iterations == nil {
		iterations = self.iterations
	}
	hash := pbkdf2.Key(password, salt, iterations, 256, sha256.New())
	encoded := base64.StdEncoding.EncodeToString(hash)
	return fmt.Sprintf("%s$%d$%s$%s", self.Name(), strconv.Iota(iterations), salt, encoded)
}
